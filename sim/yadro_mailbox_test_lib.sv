`include "yadro_mailbox_example_tb.sv"


// Base Test
class yadro_mailbox_example_base_test extends uvm_test;

  `uvm_component_utils(yadro_mailbox_example_base_test)

  yadro_mailbox_example_tb yadro_mailbox_example_tb0;
  uvm_table_printer printer;
  bit test_pass = 1;

  function new(string name = "yadro_mailbox_example_base_test", 
    uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    // Enable transaction recording for everything
    uvm_config_db#(int)::set(this, "*", "recording_detail", UVM_FULL);
    // Create the tb
    yadro_mailbox_example_tb0 = yadro_mailbox_example_tb::type_id::create("yadro_mailbox_example_tb0", this);
    // Create a specific depth printer for printing the created topology
    printer = new();
    printer.knobs.depth = 3;
  endfunction : build_phase

  function void end_of_elaboration_phase(uvm_phase phase);
    // Set verbosity for the bus monitor for this demo
//     if(yadro_mailbox_example_tb0.yadro_mailbox0.bus_monitor != null)
//       yadro_mailbox_example_tb0.yadro_mailbox0.bus_monitor.set_report_verbosity_level(UVM_FULL);
    `uvm_info(get_type_name(),
      $sformatf("Printing the test topology :\n%s", this.sprint(printer)), UVM_LOW)
  endfunction : end_of_elaboration_phase

  task run_phase(uvm_phase phase);
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 50);
  endtask : run_phase

  function void extract_phase(uvm_phase phase);
//    if(yadro_mailbox_example_tb0.scoreboard0.sbd_error)
//      test_pass = 1'b0;
  endfunction // void
  
  function void report_phase(uvm_phase phase);
    if(test_pass) begin
      `uvm_info(get_type_name(), "** UVM TEST PASSED **", UVM_NONE)
      `uvm_info(get_type_name(), "** HERE", UVM_NONE)
    end
    else begin
      `uvm_error(get_type_name(), "** UVM TEST FAIL **")
    end
    $stop;
  endfunction

endclass : yadro_mailbox_example_base_test


// test1
class test1 extends yadro_mailbox_example_base_test;
	
	local string		 		inst_name;
	
	apb_config cfg;
	
  `uvm_component_utils(test1)

  function new(string name = "test1", uvm_component parent=null);
    super.new(name,parent);
	  `uvm_info(get_type_name(), "AAAA", UVM_NONE)
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
  begin
	  super.build_phase(phase);
	   `uvm_info(get_type_name(), "BBBB", UVM_NONE)
	  for (int i = 0; i < 8; i++ ) begin
		  		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d].master[0].sequencer.run_phase", i);
		      uvm_config_db#(uvm_object_wrapper)::set(this,
		    	inst_name, 
			       "default_sequence",
				yadro_mailbox_write_byte_seq::type_id::get());
		 		
		 		
		 		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d].slave[0].sequencer.run_phase", i);
		      uvm_config_db#(uvm_object_wrapper)::set(this,
		    	inst_name, 
			       "default_sequence",
				yadro_mailbox_rd_ctrl_reg_seq::type_id::get());
		 		
		 		
		 		
		 		cfg = apb_config::type_id::create("cfg");
		  		cfg.num_masters 	= 1;
		  		cfg.num_slaves 	= 1;
		  		cfg.N 				= `N;
		  		cfg.p_index 		= i;
		  		cfg.master_configs[0] = apb_master_config::type_id::create("master_configs[0]");
		  		cfg.master_configs[0].N 		= `N;
		      cfg.master_configs[0].p_index = i;
		  		cfg.slave_configs[0] = apb_slave_config::type_id::create("slave_configs[0]");
		  		cfg.slave_configs[0].N 			= `N;
		  		cfg.slave_configs[0].p_index 	= i;
		  		
		  		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d]", i);
		 		uvm_config_object::set(this, inst_name, "cfg", cfg);
//		  		`uvm_info(get_type_name(), "CCCC", UVM_NONE)
	  end
    
  end
  endfunction : build_phase

endclass : test1



// test2
class test2 extends yadro_mailbox_example_base_test;
	
	local string		 		inst_name;
	
	apb_config cfg;
	
  `uvm_component_utils(test2)

  function new(string name = "test2", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
  begin
	  super.build_phase(phase);
	  for (int i = 0; i < 8; i++ ) begin
		  		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d].master[0].sequencer.run_phase", i);
		      uvm_config_db#(uvm_object_wrapper)::set(this,
		    	inst_name, 
			       "default_sequence",
				yadro_mailbox_write_byte_seq_mass::type_id::get());
		 		
		 		
		 		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d].slave[0].sequencer.run_phase", i);
		      uvm_config_db#(uvm_object_wrapper)::set(this,
		    	inst_name, 
			       "default_sequence",
				yadro_mailbox_rd_ctrl_reg_seq::type_id::get());
		 		
		 		
		 		
		 		cfg = apb_config::type_id::create("cfg");
		  		cfg.num_masters 	= 1;
		  		cfg.num_slaves 	= 1;
		  		cfg.N 				= `N;
		  		cfg.p_index 		= i;
		  		cfg.master_configs[0] = apb_master_config::type_id::create("master_configs[0]");
		  		cfg.master_configs[0].N 		= `N;
		      cfg.master_configs[0].p_index = i;
		  		cfg.slave_configs[0] = apb_slave_config::type_id::create("slave_configs[0]");
		  		cfg.slave_configs[0].N 			= `N;
		  		cfg.slave_configs[0].p_index 	= i;
		  		
		  		$sformat(inst_name,"yadro_mailbox_example_tb0.yadro_mailbox0.apb_env_inst[%0d]", i);
		 		uvm_config_object::set(this, inst_name, "cfg", cfg);
//		  		`uvm_info(get_type_name(), "CCCC", UVM_NONE)
	  end
    
  end
  endfunction : build_phase

endclass : test2
