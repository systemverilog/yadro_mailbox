#!/usr/bin/tclsh
set QUESTA_HOME 						"C:/questasim64_10.4a/"
set UVM_DPI_HOME 						${QUESTA_HOME}/uvm-1.2/win64/						
set USES_PRECOMPILED_UVM_DPI 			1

set UVM_HOME							${QUESTA_HOME}uvm-1.2		
set INCDIR_DEF 						"+incdir+${UVM_HOME}/src+../rtl+../sv+../sv/yadro_mailbox+../sv/apb+../sv/ic"
set VLOG_CMD							"vlog +acc -L ${QUESTA_HOME}uvm-1.2 ${INCDIR_DEF}"
set USES_DPI 							1
#set UVM_TESTNAME 						test1
set UVM_TESTNAME 						test2

proc comp {} {
	global VLOG_CMD
	
	eval exec vlib work
	eval ${VLOG_CMD} ../sv/ic/ic_pkg.sv
	eval ${VLOG_CMD} ../sv/apb/apb_pkg.sv
	eval ${VLOG_CMD} ../sv/yadro_mailbox/yadro_mailbox_pkg.sv
	eval ${VLOG_CMD} ../rtl/mem/dpram_param.v
	eval ${VLOG_CMD} ../rtl/mem/sfifo_custom.sv
	eval ${VLOG_CMD} ../rtl/cmn/readdata_mux_param.sv
	eval ${VLOG_CMD} ../rtl/mailbox_ic.sv
	eval ${VLOG_CMD} ../rtl/mailbox_apb_controller.sv
	eval ${VLOG_CMD} ../rtl/mailbox_block.sv
	eval ${VLOG_CMD} ../rtl/yadro_mailbox_top.sv
	
	eval ${VLOG_CMD} yadro_mailbox_tb_top.sv
	
	vopt -L work yadro_mailbox_tb_top -o yadro_mailbox_tb_top_opt
}

proc runnn {} {
	global VSIM
	global UVM_TESTNAME
	global QUESTA_HOME
	global UVM_DPI_HOME
	
	comp
	eval vsim -L ${QUESTA_HOME}uvm-1.2 -assertdebug -voptargs="+acc" +UVM_TESTNAME=${UVM_TESTNAME} -sv_lib ${UVM_DPI_HOME}uvm_dpi +UVM_CONFIG_DB_TRACE -l run.log yadro_mailbox_tb_top_opt
#	add wave -position insertpoint sim:/yadro_mailbox_tb_top/arbiter_inst/*
}

proc all {} {
	runnn
}
