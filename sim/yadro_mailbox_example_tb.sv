`include "apb_master_seq_lib.sv"
`include "apb_slave_seq_lib.sv"
`include "yadro_mailbox_seq_lib.sv"

//------------------------------------------------------------------------------
//
// CLASS: yadro_mailbox_example_tb
//
//------------------------------------------------------------------------------

class yadro_mailbox_example_tb extends uvm_env;

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils(yadro_mailbox_example_tb)

  // yadro_mailbox environment
  yadro_mailbox_env#(.W(`W), .N(`N), .K(`K), .APB_ADDR_WDTH(`APB_ADDR_WDTH))  yadro_mailbox0;

  // Scoreboard to check the memory operation of the slave.
//  yadro_mailbox_example_scoreboard scoreboard0;

  // new
  function new (string name, uvm_component parent=null);
    super.new(name, parent);
  endfunction : new

  // build_phase
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
//    uvm_config_db#(int)::set(this,"yadro_mailbox0", 
//			       "num_masters", 1);
//    uvm_config_db#(int)::set(this,"yadro_mailbox0", 
//			       "num_slaves", 1);
    
    yadro_mailbox0 = yadro_mailbox_env#(.W(`W), .N(`N), .K(`K), .APB_ADDR_WDTH(`APB_ADDR_WDTH))::type_id::create("yadro_mailbox0", this);
//    scoreboard0 = yadro_mailbox_example_scoreboard::type_id::create("scoreboard0", this);
  endfunction : build_phase

  function void connect_phase(uvm_phase phase);
    // Connect slave0 monitor to scoreboard
//    yadro_mailbox0.slaves[0].monitor.item_collected_port.connect(
//      scoreboard0.item_collected_export);
  endfunction : connect_phase

  function void end_of_elaboration_phase(uvm_phase phase);
    // Set up slave address map for yadro_mailbox0 (basic default)
//    yadro_mailbox0.set_slave_address_map("slaves[0]", 0, 16'hffff);
  endfunction : end_of_elaboration_phase

endclass : yadro_mailbox_example_tb


