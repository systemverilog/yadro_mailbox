`include "yadro_mailbox_pkg.sv"
`include "yadro_mailbox_top.sv"
`include "yadro_mailbox_if.sv"

module yadro_mailbox_tb_top;
  import uvm_pkg::*;
  import apb_pkg::*;
  import ic_pkg::*;	
  import yadro_mailbox_pkg::*;
  
  `include "yadro_mailbox_test_lib.sv"

  	yadro_mailbox_if#(.W(`W), .N(`N), .K(`K), .APB_ADDR_WDTH(`APB_ADDR_WDTH)) 	 yadro_mailbox_vif(); 

	logic [`N*`APB_ADDR_WDTH-1:0] PADDR;
	logic [`N*`W-1:0] 				PRDATA;
	logic [`N*`W-1:0] 				PWDATA;
	logic [`N-1:0] 					PSEL;
	logic [`N-1:0] 					PENABLE;
	logic [`N-1:0] 					PWRITE;
	logic [`N-1:0] 					PREADY;
	logic [`N-1:0] 					PSLVERR;
	logic [`N-1:0] 					interrupt;
	
	genvar p_i;
	generate 	
		for (p_i = 0; p_i < `N; p_i++) begin : p_i_inst
			assign PADDR[p_i*`APB_ADDR_WDTH + `APB_ADDR_WDTH - 1: p_i*`APB_ADDR_WDTH] 				= yadro_mailbox_vif.apbN_if[p_i].PADDR;
			assign PWDATA[p_i*`W + `W - 1: p_i*`W] 					= yadro_mailbox_vif.apbN_if[p_i].PWDATA;
			assign PSEL[p_i]													= yadro_mailbox_vif.apbN_if[p_i].PSEL;
			assign PENABLE[p_i] 												= yadro_mailbox_vif.apbN_if[p_i].PENABLE;
			assign PWRITE[p_i] 												= yadro_mailbox_vif.apbN_if[p_i].PWRITE;
			assign yadro_mailbox_vif.apbN_if[p_i].PRDATA				= PRDATA[p_i*`W + `W - 1: p_i*`W];
			assign yadro_mailbox_vif.apbN_if[p_i].PREADY 			= PREADY[p_i];
			assign yadro_mailbox_vif.apbN_if[p_i].PSLVERR			= PSLVERR[p_i];
			assign yadro_mailbox_vif.icN_if[p_i].interrupt 			= interrupt[p_i];
		end	
	endgenerate

	yadro_mailbox_top #(
				.W 				(`W),
				.N 				(`N),
				.K 				(`K),
				.APB_ADDR_WDTH (`APB_ADDR_WDTH),
				.N_ADDR_WDTH 	(`N_ADDR_WDTH),
				.NxN_ADDR_WDTH (`NxN_ADDR_WDTH),
				.K_ADDR_WDTH 	(`K_ADDR_WDTH)
	) yadro_mailbox_top_inst (
		.clk			(yadro_mailbox_vif.clk),
		.reset		(yadro_mailbox_vif.reset),
		.PADDR		(PADDR),
		.PRDATA		(PRDATA),
		.PWDATA		(PWDATA),
		.PSEL			(PSEL),
		.PENABLE		(PENABLE),
		.PWRITE		(PWRITE),
		.PREADY		(PREADY),
		.PSLVERR		(PSLVERR),
		.interrupt	(interrupt)
	);

//	assign yadro_mailbox_vif.out.t_ready = 1'b1;
	
  initial begin automatic uvm_coreservice_t cs_ = uvm_coreservice_t::get();
    uvm_config_db#(virtual yadro_mailbox_if#(.W(`W), .N(`N), .K(`K), .APB_ADDR_WDTH(`APB_ADDR_WDTH)))::set(cs_.get_root(), "*", "yadro_mailbox_vif", yadro_mailbox_vif);
    run_test();
  end

  initial begin
    yadro_mailbox_vif.reset <= 1'b1;
    yadro_mailbox_vif.clk <= 1'b1;
    #51 yadro_mailbox_vif.reset = 1'b0;
  end

  //Generate Clock
  always
    #5 yadro_mailbox_vif.clk = ~yadro_mailbox_vif.clk;

endmodule
