`ifndef __MAILBOX_APB_CONTROLLER__
`define __MAILBOX_APB_CONTROLLER__

module mailbox_apb_controller #(
	parameter W 				= 32,
	parameter N 				= 32,
	parameter APB_ADDR_WDTH = 32,
	parameter N_ADDR_WDTH	= 5,
	parameter NxN_ADDR_WDTH	= 5
)(
	input logic 						clk,
	input logic 						reset,
	apb_if								apb,
	output logic						wr_req,
	output logic						rd_req,
	input logic	[N_ADDR_WDTH-1:0]	p_index,
	input logic [N-1:0]				wr_pready,
	input logic [N-1:0]				wr_pslverr,
	input logic	[W*N-1:0]			rd_prdata,
	input logic [N-1:0]				rd_pready,
	input logic [N-1:0]				rd_pslverr,
	input logic [N-1:0]				ctrl_reg
);

// ------------------------------------------
// Internal Parameters
// ------------------------------------------

// ------------------------------------------
// Internal Signals
// ------------------------------------------
logic 				wr_req0;
logic 				rd_req0;	
logic [W-1:0]		prdata_muxed;	
// ------------------------------------------
// Code
// ------------------------------------------
assign valid_ctrl_reg_addr = ~|(apb.PADDR[APB_ADDR_WDTH-1:NxN_ADDR_WDTH+1]) & (apb.PADDR[NxN_ADDR_WDTH] == 1'b1) & 
			~|(apb.PADDR[NxN_ADDR_WDTH-1:N_ADDR_WDTH]) & (apb.PADDR[N_ADDR_WDTH-1:0] == p_index);

//wr_req
assign wr_req = apb.PSEL & apb.PENABLE & apb.PWRITE & ~wr_req0;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		wr_req0 <= 1'b0;
	else if (apb.PREADY)
		wr_req0 <= 1'b0;
	else if (wr_req)
		wr_req0 <= 1'b1;

//rd_req
assign rd_req = apb.PSEL & apb.PENABLE & ~apb.PWRITE & ~rd_req0;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		rd_req0 <= 1'b0;
	else if (apb.PREADY)
		rd_req0 <= 1'b0;
	else if (rd_req)
		rd_req0 <= 1'b1;

// ------------------------------------------
// APB outputs
// ------------------------------------------
always_ff @ (posedge clk or posedge reset)
	if (reset)
		apb.PREADY <= 1'b0;
	else if (apb.PREADY)
		apb.PREADY <= 1'b0;
	else
		apb.PREADY <= wr_req0 | (rd_req0 & (valid_ctrl_reg_addr | (|rd_pready) ) );

always_ff @ (posedge clk or posedge reset)
	if (reset)
		apb.PSLVERR <= 1'b0;
	else if (apb.PREADY)
		apb.PSLVERR <= 1'b0;
	else
		apb.PSLVERR <= |wr_pslverr | |rd_pslverr | (wr_req0 & ~|wr_pready) | (rd_req0 & ~valid_ctrl_reg_addr & ~|rd_pready);

readdata_mux_param #(
	.NUM_INPUTS(N),
	.DOUT_WDTH(W)
) dsp_hdr_mux (
	.din				(rd_prdata),
	.sel_onehot		(rd_pready),
	.dout				(prdata_muxed)
);

always_ff @ (posedge clk or posedge reset)
	if (reset)
		apb.PRDATA <= 1'b0;
	else if (rd_req0 & valid_ctrl_reg_addr)
		apb.PRDATA <= ctrl_reg;
	else if (rd_req0 & (|rd_pready))
		apb.PRDATA <= prdata_muxed;

endmodule
`endif


