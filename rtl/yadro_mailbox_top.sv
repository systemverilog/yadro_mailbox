`ifndef __YADRO_MAILBOX_TOP__
`define __YADRO_MAILBOX_TOP__

`ifdef VERBOSE
	`include "yadro_mailbox_macros.sv"
`else
	`include "yadro_mailbox_defines.sv"
`endif

module yadro_mailbox_top #(
	parameter W 				= 32,
	parameter N 				= 32,
	parameter K 				= 64,
	parameter APB_ADDR_WDTH = 32,
	parameter N_ADDR_WDTH	= 5,
	parameter NxN_ADDR_WDTH	= 5,
	parameter K_ADDR_WDTH	= 7
) (
	input logic 							clk,
  	input logic 							reset,
  	
  	input logic [N*APB_ADDR_WDTH-1:0] 	PADDR,
  	output logic [N*W-1:0] 					PRDATA,
  	input logic [N*W-1:0] 					PWDATA,
  	input logic [N-1:0] 						PSEL,
  	input logic [N-1:0] 						PENABLE,
  	input logic [N-1:0] 						PWRITE,
  	output logic [N-1:0] 					PREADY,
  	output logic [N-1:0] 					PSLVERR,
  	
  	output logic [N-1:0]						interrupt
);

apb_if #(.DATA_WIDTH(W), .ADDR_WIDTH(`APB_ADDR_WDTH))	apbN_if[N-1:0]();
logic	[N-1:0]					wr_req;
logic	[N-1:0]					rd_req;
	
logic	[N-1:0]					wr_pready[N];	
logic	[N-1:0]					wr_pslverr[N];
logic	[W*N-1:0]				rd_prdata[N];	
logic	[N-1:0]					rd_pready[N];	
logic	[N-1:0]					rd_pslverr[N];	
logic	[N-1:0]					ctrl_reg[N];

genvar p_i;
genvar p_j;	
generate 	
	for (p_i = 0; p_i < N; p_i++) begin : p_i_inst
		
		assign apbN_if[p_i].PADDR				= PADDR[p_i*APB_ADDR_WDTH + APB_ADDR_WDTH - 1: p_i*APB_ADDR_WDTH];
		assign apbN_if[p_i].PWDATA				= PWDATA[p_i*W + W - 1: p_i*W];
		assign apbN_if[p_i].PSEL				= PSEL[p_i];
		assign apbN_if[p_i].PENABLE			= PENABLE[p_i];
		assign apbN_if[p_i].PWRITE				= PWRITE[p_i];
		assign PRDATA[p_i*W + W - 1: p_i*W]	= apbN_if[p_i].PRDATA;
		assign PREADY[p_i]						= apbN_if[p_i].PREADY;
		assign PSLVERR[p_i]						= apbN_if[p_i].PSLVERR;
		
		for (p_j = 0; p_j < N; p_j++) begin : p_j_inst
		  	mailbox_block #(
				.W 	(W),
				.N 	(N),
				.K 	(K),
				.APB_ADDR_WDTH (APB_ADDR_WDTH),
				.NxN_ADDR_WDTH (NxN_ADDR_WDTH),
				.K_ADDR_WDTH 	(K_ADDR_WDTH)
			) mailbox_block_inst (
				.clk			(clk),
			  	.reset		(reset),
			  	.apb_wr		(apbN_if[p_j]),
			  	.apb_rd		(apbN_if[p_i]),
			  	.block_addr	(N*p_i+p_j),
			  	.wr_req 		(wr_req[p_j]),
			  	.rd_req 		(rd_req[p_i]),
			  	
			  	.wr_pready 		(wr_pready[p_j][p_i]),
			  	.wr_pslverr 	(wr_pslverr[p_j][p_i]),
			  	.rd_prdata		(rd_prdata[p_i][p_j*W + W - 1: p_j*W]),
			  	.rd_pready 		(rd_pready[p_i][p_j]),
			  	.rd_pslverr 	(rd_pslverr[p_i][p_j]),
			  	.ctrl_reg		(ctrl_reg[p_i][p_j])
			);
		end
		
		mailbox_apb_controller #(
			.W					(W),
			.N 				(N),
			.N_ADDR_WDTH 	(N_ADDR_WDTH),
			.NxN_ADDR_WDTH (NxN_ADDR_WDTH)
		) mailbox_apb_controller_inst (
			.clk			(clk),
		  	.reset		(reset),
		  	.apb			(apbN_if[p_i]),
		  	.wr_req		(wr_req[p_i]),
		  	.rd_req		(rd_req[p_i]),
		  	.p_index		(p_i),
		  	.wr_pready	(wr_pready[p_i]),
		  	.wr_pslverr	(wr_pslverr[p_i]),
		  	.rd_prdata 	(rd_prdata[p_i]),
		  	.rd_pready	(rd_pready[p_i]),
		  	.rd_pslverr	(rd_pslverr[p_i]),
		  	.ctrl_reg	(ctrl_reg[p_i])
		);
		
		
		mailbox_ic #(
			.N 	(N)
		) mailbox_ic_inst (
			.clk			(clk),
		  	.reset		(reset),
		  	.ctrl_reg	(ctrl_reg[p_i]),
		  	.interrupt	(interrupt[p_i])
		);
		
	end	
endgenerate

endmodule
`endif