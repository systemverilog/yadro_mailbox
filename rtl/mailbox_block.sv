`ifndef __MAILBOX_BLOCK__
`define __MAILBOX_BLOCK__

module mailbox_block #(
	parameter W 				= 32,
	parameter N 				= 32,
	parameter K 				= 64,
	parameter APB_ADDR_WDTH = 32,
	parameter NxN_ADDR_WDTH	= 5,
	parameter K_ADDR_WDTH	= 7
)(
	input logic 						clk,
	input logic 						reset,
	apb_if								apb_wr,
	apb_if								apb_rd,
	input logic	[NxN_ADDR_WDTH-1:0]	block_addr,
	input logic							wr_req,
	input logic							rd_req,
	output logic						wr_pready,
	output logic						wr_pslverr,
	output logic [W-1:0]				rd_prdata,
	output logic						rd_pready,
	output logic						rd_pslverr,
	output logic						ctrl_reg
);

// ------------------------------------------
// Internal Parameters
// ------------------------------------------

// ------------------------------------------
// Internal Signals
// ------------------------------------------
logic 		valid_wr_addr;
logic			apb_wr_cond;
logic 		valid_rd_addr;	
logic			apb_rd_cond;	
logic			fifo_wr_en;
logic			fifo_rd_en;	
logic			fifo_wrfull;
logic			fifo_rdempty;
logic			fifo_rdempty_val0;
logic			ctrl_reg_clr;	
// ------------------------------------------
// Code
// ------------------------------------------
assign valid_wr_addr = ~|(apb_wr.PADDR[APB_ADDR_WDTH-1:NxN_ADDR_WDTH]) & (apb_wr.PADDR[NxN_ADDR_WDTH-1:0] == block_addr);
assign apb_wr_cond = wr_req & valid_wr_addr;
assign valid_rd_addr = ~|(apb_rd.PADDR[APB_ADDR_WDTH-1:NxN_ADDR_WDTH]) & (apb_rd.PADDR[NxN_ADDR_WDTH-1:0] == block_addr);	
assign apb_rd_cond = rd_req & valid_rd_addr;	
assign fifo_wr_en		= apb_wr_cond & ~fifo_wrfull;	
assign fifo_rd_en		= apb_rd_cond & ~fifo_rdempty;	

sfifo_custom #(
	.pWDATA_W	   	(W),
  	.pWADDR_W	   	(K_ADDR_WDTH),
  	.pRDATA_W    		(W),
  	.pRADDR_W   		(K_ADDR_WDTH)
) mailbox_block_fifo_inst (
	.reset			( reset ),
	//
	.wrclk 			( clk ),
	.wrcen 			( 1'b1 ),
	.wrreq 			( fifo_wr_en),
	.wrdata 			( apb_wr.PWDATA ),
	.wrfull 			( fifo_wrfull ),
	.wrempty 		(  ),
	.wrusedw			(  ),
	//
	.rdclk 			( clk ),
	.rdcen 			( 1'b1 ),
	.rdreq 			( fifo_rd_en ),
	.rddata 			( rd_prdata ),
	.rdempty 		( fifo_rdempty ),
	.rdfull 			(  ),
	.rdusedw			(  )
);

// ------------------------------------------
// APB outputs
// ------------------------------------------
always_ff @ (posedge clk or posedge reset)
	if (reset)
		wr_pready 	<= 1'b0;
	else if (apb_wr_cond)
		wr_pready 	<= 1'b1;
	else
		wr_pready 	<= 1'b0;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		wr_pslverr <= 1'b0;
	else if (apb_wr_cond && fifo_wrfull)
		wr_pslverr <= 1'b1;
	else
		wr_pslverr <= 1'b0;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		rd_pready 	<= 1'b0;
	else if (apb_rd_cond)
		rd_pready 	<= 1'b1;
	else
		rd_pready 	<= 1'b0;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		rd_pslverr <= 1'b0;
	else if (apb_rd_cond && fifo_rdempty)
		rd_pslverr <= 1'b1;
	else
		rd_pslverr <= 1'b0;
// ------------------------------------------
// Control reg 
// ------------------------------------------
always_ff @ (posedge clk or posedge reset)
	if (reset) begin
		fifo_rdempty_val0 <= 1'b0;
	end else begin
		fifo_rdempty_val0 <= fifo_rdempty;
	end

assign ctrl_reg_clr = ~fifo_rdempty_val0 & fifo_rdempty;

always_ff @ (posedge clk or posedge reset)
	if (reset)
		ctrl_reg <= 1'b0;
	else if (apb_wr_cond && !fifo_wrfull)
		ctrl_reg <= 1'b1;
	else if (ctrl_reg_clr)
		ctrl_reg <= 1'b0;


endmodule
`endif


