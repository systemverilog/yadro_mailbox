`ifndef __READDATA_MUX_PARAM__
`define __READDATA_MUX_PARAM__

module readdata_mux_param #(
	parameter NUM_INPUTS 	= 8,
	parameter DOUT_WDTH 	= 64
)(
	input logic [NUM_INPUTS*DOUT_WDTH-1:0]	din,
	input logic [NUM_INPUTS-1:0]			sel_onehot,
	output logic [DOUT_WDTH-1:0]			dout
);

logic [DOUT_WDTH-1:0]	map[NUM_INPUTS];

genvar inputs_num;
generate
	for (inputs_num=0; inputs_num < NUM_INPUTS; inputs_num++) begin : loop
		assign map[inputs_num] = din[inputs_num*DOUT_WDTH+DOUT_WDTH-1:inputs_num*DOUT_WDTH];
	end
endgenerate

always_comb begin
	dout = 'x;
	for (int unsigned i=0; i < NUM_INPUTS; i++) begin
		if (sel_onehot[i]) dout = map[i];
	end
end

endmodule
`endif

