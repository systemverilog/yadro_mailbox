`ifndef __PIPELINED_OR__
`define __PIPELINED_OR__

module pipelined_or #(
	parameter N 				= 32,
	parameter OR_WDTH 		= 4,
	parameter STAGES_NUM 	= 1
	
)(
	input logic 						clk,
	input logic 						reset,
	input logic [N-1:0]				in,
	output logic 						out
	
);

localparam OR_NUM_L1 = N/OR_WDTH;
localparam OR_NUM_L2 = OR_NUM_L1/OR_WDTH;
localparam OR_NUM_L3 = OR_NUM_L2/OR_WDTH;
localparam OR_NUM_L4 = OR_NUM_L3/OR_WDTH;
	
// ------------------------------------------
// Internal Parameters
// ------------------------------------------

// ------------------------------------------
// Internal Signals
// ------------------------------------------

// ------------------------------------------
// Code
// ------------------------------------------
genvar i;
genvar p_j;	
generate 	
	for (i = 0; i < OR_NUM; i++) begin : i_inst
		assign or_lvl1[i] = 
		
	end	
endgenerate


endmodule
`endif


