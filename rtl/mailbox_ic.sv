`ifndef __MAILBOX_IC__
`define __MAILBOX_IC__

module mailbox_ic #(
	parameter N 				= 32
)(
	input logic 						clk,
	input logic 						reset,
	input logic [N-1:0]				ctrl_reg,
	output logic 						interrupt
	
);

// ------------------------------------------
// Internal Parameters
// ------------------------------------------

// ------------------------------------------
// Internal Signals
// ------------------------------------------

// ------------------------------------------
// Code
// ------------------------------------------
always_ff @ (posedge clk or posedge reset)
	if (reset)
		interrupt <= 1'b0;
	else
		interrupt <= |ctrl_reg;


endmodule
`endif


