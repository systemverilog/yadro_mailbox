/*



  parameter int  pWDATA_W = 8;
  parameter int  pWADDR_W = 6;
  parameter int  pRDATA_W = 2;
  parameter int  pRADDR_W = 8;



  logic                  sfifo__reset  ;
  logic                  sfifo__wrclk  ;
  logic                  sfifo__wrcen  ;
  logic                  sfifo__wrreq  ;
  logic [pWDATA_W-1 : 0] sfifo__wrdata ;
  logic                  sfifo__wrfull ;
  logic                  sfifo__wrempty;
  logic [pWADDR_W-1 : 0] sfifo__wrusedw;
  logic                  sfifo__rdclk  ;
  logic                  sfifo__rdcen  ;
  logic                  sfifo__rdreq  ;
  logic [pRDATA_W-1 : 0] sfifo__rddata ;
  logic                  sfifo__rdfull ;
  logic                  sfifo__rdempty;
  logic [pRADDR_W-1 : 0] sfifo__rdusedw;



  sfifo
  #(
    .pWDATA_W ( 8 ) ,
    .pWADDR_W ( 8 ) ,
    .pRDATA_W ( 8 ) ,
    .pRADDR_W ( 8 )
  )
  sfifo
  (
    .reset   ( sfifo__reset   ) ,
    .wrclk   ( sfifo__wrclk   ) ,
    .wrcen   ( sfifo__wrcen   ) ,
    .wrreq   ( sfifo__wrreq   ) ,
    .wrdata  ( sfifo__wrdata  ) ,
    .wrusedw ( sfifo__wrusedw ) ,
    .wrfull  ( sfifo__wrfull  ) ,
    .wrempty ( sfifo__wrempty ) ,
    .rdclk   ( sfifo__rdclk   ) ,
    .rdcen   ( sfifo__rdcen   ) ,
    .rdreq   ( sfifo__rdreq   ) ,
    .rddata  ( sfifo__rddata  ) ,
    .rdempty ( sfifo__rdempty ) ,
    .rdfull  ( sfifo__rdfull  ) ,
    .rdusedw ( sfifo__rdusedw )
  );


  assign sfifo__reset  = '0 ;
  assign sfifo__wrclk  = '0 ;
  assign sfifo__wrcen  = '0 ;
  assign sfifo__wrreq  = '0 ;
  assign sfifo__wrdata = '0 ;
  assign sfifo__rdclk  = '0 ;
  assign sfifo__rdcen  = '0 ;
  assign sfifo__rdreq  = '0 ;



*/


//------------------------------------------------------------------------------------------------------
// dual clock fifo with data width conversion (dwc) and syncronius compare logic
//------------------------------------------------------------------------------------------------------

module sfifo_custom
#(
  parameter int  pWDATA_W = 8 ,
  parameter int  pWADDR_W = 6 ,
  parameter int  pRDATA_W = 2 ,
  parameter int  pRADDR_W = 8
)
(
  reset   ,
  //
  wrclk   ,
  wrclr	  ,
  wrcen   ,
  wrreq   ,
  wrdata  ,
  wrfull  ,
  wrempty ,
  wrusedw ,
  //
  rdclk   ,
  rdclr	  ,
  rdcen   ,
  rdreq   ,
  rddata  ,
  rdempty ,
  rdfull  ,
  rdusedw
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  input  logic                  reset    ;
  //
  input  logic                  wrclk    ;
  input  logic                  wrclr    ;
  input  logic                  wrcen    ;
  input  logic                  wrreq    ;
  input  logic [pWDATA_W-1 : 0] wrdata   ;
  output logic                  wrfull   ;
  output logic                  wrempty  ;
  output logic [pWADDR_W-1 : 0] wrusedw  ;
  //
  input  logic                  rdclk    ;
  input  logic                  rdclr    ;
  input  logic                  rdcen    ;
  input  logic                  rdreq    ;
  output logic [pRDATA_W-1 : 0] rddata   ;
  output logic                  rdempty  ;
  output logic                  rdfull   ;
  output logic [pRADDR_W-1 : 0] rdusedw  ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  //
  // pointers
  //
  logic [pWADDR_W : 0] wrptr_bin  ; // + 1 bit for full/empty decode
  logic [pWADDR_W : 0] wrptr_gray ;

  logic [pRADDR_W : 0] rdptr_bin  ; // + 1 bit for full/empty decode
  logic [pRADDR_W : 0] rdptr_gray ;
  //
  // altsyncram remaped wires
  //
  logic                rdclock   ;
  logic                rden      ;
  logic [pRADDR_W-1:0] rdaddress ;
  logic [pRDATA_W-1:0] q         ;

  logic                wrclock   ;
  logic                wren      ;
  logic [pWADDR_W-1:0] wraddress ;
  logic [pWDATA_W-1:0] data      ;
  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  //
  // write ram port
  //
  assign wrclock    = wrclk;
  assign wren       = wrreq & wrcen;
  assign wraddress  = wrptr_bin[pWADDR_W-1:0];
  assign data       = wrdata;
  //
  // read ram port
  //
  assign rdclock    = rdclk ;
  assign rden       = rdreq & rdcen;
  assign rdaddress  = rdptr_bin[pRADDR_W-1:0];
  assign rddata     = q;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  sfifo_wrptr
  #(
    .pWADDR_W ( pWADDR_W ) ,
    .pRADDR_W ( pRADDR_W )
  )
  wrptr
  (
    .reset      ( reset      ) ,
    .wrclk      ( wrclk      ) ,
    .wrclr      ( wrclr      ) , 
    .wrcen      ( wrcen      ) ,
    .wrreq      ( wrreq      ) ,
    //
    .rdptr_gray ( rdptr_gray ) ,
    .wrptr_bin  ( wrptr_bin  ) ,
    .wrptr_gray ( wrptr_gray ) ,
    //
    .wrempty    ( wrempty    ) ,
    .wrfull     ( wrfull     ) ,
    .wrusedw    ( wrusedw    )
  );
  //
  //
  //
  sfifo_rdptr
  #(
    .pWADDR_W ( pWADDR_W ) ,
    .pRADDR_W ( pRADDR_W )
  )
  rdptr
  (
    .reset       ( reset      ),
    .rdclk       ( rdclk      ),
    .rdclr       ( rdclr      ),
    .rdcen       ( rdcen      ),
    .rdreq       ( rdreq      ),
    //
    .wrptr_gray  ( wrptr_gray ),
    .rdptr_bin   ( rdptr_bin  ),
    .rdptr_gray  ( rdptr_gray ),
    //
    .rdempty     ( rdempty    ),
    .rdfull      ( rdfull     ),
    .rdusedw     ( rdusedw    )
  );
  //
  // ram use unregistred output
  //

//  altsyncram
//  altsyncram_component (
//        .wren_a         ( wren            ),
//        .clock0         ( wrclock         ),
//        .clock1         ( rdclock         ),
//        .address_a      ( wraddress       ),
//        .address_b      ( rdaddress       ),
//        .rden_b         ( rden            ),
//        .data_a         ( data            ),
//        .q_b            ( q               ),
//        .aclr0          ( 1'b0            ),
//        .aclr1          ( 1'b0            ),
//        .addressstall_a ( 1'b0            ),
//        .addressstall_b ( 1'b0            ),
//        .byteena_a      ( 1'b1            ),
//        .byteena_b      ( 1'b1            ),
//        .clocken0       ( 1'b1            ),
//        .clocken1       ( 1'b1            ),
//        .clocken2       ( 1'b1            ),
//        .clocken3       ( 1'b1            ),
//        .data_b         ( {pRDATA_W{1'b1}}),
//        .eccstatus      (                 ),
//        .q_a            (                 ),
//        .rden_a         ( 1'b1            ),
//        .wren_b         ( 1'b0            )
//  );
//
//  defparam
//    altsyncram_component.address_reg_b          = "CLOCK1",
//    altsyncram_component.clock_enable_input_a   = "BYPASS",
//    altsyncram_component.clock_enable_input_b   = "BYPASS",
//    altsyncram_component.clock_enable_output_a  = "BYPASS",
//    altsyncram_component.clock_enable_output_b  = "BYPASS",
////`ifdef MODEL_TECH
//    altsyncram_component.intended_device_family = "Cyclone V",
////`endif
//    altsyncram_component.lpm_type               = "altsyncram",
//    altsyncram_component.numwords_a             = 2**pWADDR_W,  //
//    altsyncram_component.numwords_b             = 2**pRADDR_W,  //
//    altsyncram_component.operation_mode         = "DUAL_PORT",
//    altsyncram_component.outdata_aclr_b         = "NONE",
//    altsyncram_component.outdata_reg_b          = "UNREGISTERED",
//    altsyncram_component.power_up_uninitialized = "FALSE",
//    altsyncram_component.rdcontrol_reg_b        = "CLOCK1",
//    altsyncram_component.widthad_a              = pWADDR_W,
//    altsyncram_component.widthad_b              = pRADDR_W,
//    altsyncram_component.width_a                = pWDATA_W,
//    altsyncram_component.width_b                = pRDATA_W,
//    altsyncram_component.width_byteena_a        = 1;
	 
	dpram_param #(
		.RAM_LEN		(2**pWADDR_W),
		.DATA_WDTH  (pWDATA_W),
		.ADDR_WDTH  (pWADDR_W)
	) dpram_param_inst (
			.data_a 	(data),
			.data_b 	({pRDATA_W{1'b1}}),
			.addr_a 	(wraddress),
			.addr_b 	(rdaddress),
			.we_a 	(wren),
			.we_b 	(1'b0),
			.clk_a 	(wrclock),
			.clk_b 	(rdclock),
			.q_a 		(),
			.q_b 		(q)
	); 
	 

endmodule

//------------------------------------------------------------------------------------------------------
// stream fifo read pointer and synchronious logic
//------------------------------------------------------------------------------------------------------

module sfifo_rdptr
#(
  parameter int pWADDR_W = 6 ,
  parameter int pRADDR_W = 8
)
(
  reset       ,
  rdclk       ,
  rdclr       ,
  rdcen       ,
  rdreq       ,
  wrptr_gray  ,
  rdptr_bin   ,
  rdptr_gray  ,
  rdempty     ,
  rdfull      ,
  rdusedw
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  input  logic                  reset      ;
  input  logic                  rdclk      ;
  input  logic                  rdclr      ;
  input  logic                  rdcen      ;
  input  logic                  rdreq      ;
  input  logic [pWADDR_W : 0]   wrptr_gray ; // + 1 bit for full/empty decode
  output logic [pRADDR_W : 0]   rdptr_bin  ;
  output logic [pRADDR_W : 0]   rdptr_gray ;
  output logic                  rdempty    ;
  output logic                  rdfull     ;
  output logic [pRADDR_W-1 : 0] rdusedw    ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  function automatic logic [pWADDR_W : 0] gray2bin(input logic [pWADDR_W : 0] gray);
    int i;
    for (i = 0; i < $size(gray); i++)
      gray2bin[i] = ^(gray >> i);
  endfunction

  function automatic logic [pRADDR_W : 0] bin2gray(input logic [pRADDR_W : 0] bin);
    bin2gray = (bin >> 1) ^ bin;
  endfunction

  logic [pRADDR_W : 0] rdptr_bin_next   ; // + 1 bit for full/empty decode
  logic [pRADDR_W : 0] rdptr_gray_next  ;

  logic [pRADDR_W : 0] wrptr_bin2rdptr     ;
  logic [pRADDR_W : 0] wrptr_bin2rdptr_inv ;

  logic [pWADDR_W : 0]  wrptr_bin       /* synthesis keep */;
  logic [pWADDR_W : 0]  wrptr_gray_1    ;
  logic [pWADDR_W : 0]  wrptr_gray_2    ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  //
  // binary and gray pointers
  //
  assign rdptr_bin_next   = rdptr_bin + rdreq;
  assign rdptr_gray_next  = bin2gray(rdptr_bin_next);

  always_ff @(posedge rdclk or posedge reset) begin
    if (reset) begin
      rdptr_bin   <= '0;
      rdptr_gray  <= '0;
    end else if (rdclr) begin
      rdptr_bin   <= '0;
      rdptr_gray  <= '0;
    end else if (rdcen) begin
      rdptr_bin   <= rdptr_bin_next;
      rdptr_gray  <= rdptr_gray_next;
    end
  end
  //
  // write pointer syncronizer
  //
  always_ff @(posedge rdclk) begin
    {wrptr_gray_2, wrptr_gray_1} <= {wrptr_gray_1, wrptr_gray};
  end

  assign wrptr_bin = gray2bin(wrptr_gray_2);
  //
  // convert write pointer to read pointer width for flag decode loigc
  //
  generate
    //
    // if pRADDR_W > pWADDR_W then pRDATA_W < pWDATA_W and we must to convert
    //    write pointer to read pointer with add 1'b0 at rigth
    if (pRADDR_W > pWADDR_W) begin
      assign wrptr_bin2rdptr = {wrptr_bin, {(pRADDR_W-pWADDR_W){1'b0}}};
    end
    //
    // if pRADDR_W <= pWADDR_W then pRDATA_W >= pWDATA_W and we must to convert
    //    write pointer to read pointer with cut off bits at left
    else begin
      assign wrptr_bin2rdptr = wrptr_bin[pWADDR_W : pWADDR_W-pRADDR_W];
    end
  endgenerate

  assign wrptr_bin2rdptr_inv = {~wrptr_bin2rdptr[pRADDR_W], wrptr_bin2rdptr[pRADDR_W-1 : 0]};
  //
  // flag decode logic
  //
  always_ff @(posedge rdclk or posedge reset) begin
    if (reset) begin
      rdempty <= 1'b1;
      rdfull  <= 1'b0;
      rdusedw <= '0;
    end else if (rdclr) begin
      rdempty <= 1'b1;
      rdfull  <= 1'b0;
      rdusedw <= '0;	
    end else begin
      rdempty <= (rdptr_bin_next == wrptr_bin2rdptr);
      rdfull  <= (rdptr_bin      == wrptr_bin2rdptr_inv);
      rdusedw <= (wrptr_bin2rdptr - rdptr_bin_next);
    end
  end

endmodule

//------------------------------------------------------------------------------------------------------
// stream fifo write pointer and syncronious logic
//------------------------------------------------------------------------------------------------------

module sfifo_wrptr
#(
  parameter int pWADDR_W = 6 ,
  parameter int pRADDR_W = 8
)
(
  reset       ,
  wrclk       ,
  wrclr       ,
  wrcen       ,
  wrreq       ,
  rdptr_gray  ,
  wrptr_bin   ,
  wrptr_gray  ,
  wrempty     ,
  wrfull      ,
  wrusedw
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  input  logic                  reset      ;
  input  logic                  wrclk      ;
  input  logic                  wrclr      ;
  input  logic                  wrcen      ;
  input  logic                  wrreq      ;
  input  logic [pRADDR_W : 0]   rdptr_gray ; // + 1 bit for full/empty decode
  output logic [pWADDR_W : 0]   wrptr_bin  ;
  output logic [pWADDR_W : 0]   wrptr_gray ;
  output logic                  wrempty    ;
  output logic                  wrfull     ;
  output logic [pWADDR_W-1 : 0] wrusedw    ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  function automatic logic [pRADDR_W : 0] gray2bin(input logic [pRADDR_W : 0] gray);
    int i;
    for (i = 0; i < $size(gray); i++)
      gray2bin[i] = ^(gray >> i);
  endfunction

  function automatic logic [pWADDR_W : 0] bin2gray(input logic [pWADDR_W : 0] bin);
    bin2gray = (bin >> 1) ^ bin;
  endfunction


  logic [pWADDR_W : 0] wrptr_bin_next   ;
  logic [pWADDR_W : 0] wrptr_gray_next  ;

  logic [pWADDR_W : 0] rdptr_bin2wrptr     ;
  logic [pWADDR_W : 0] rdptr_bin2wrptr_inv ;

  logic [pRADDR_W : 0] rdptr_bin        /* synthesis keep */;
  logic [pRADDR_W : 0] rdptr_gray_1     ;
  logic [pRADDR_W : 0] rdptr_gray_2     ;

  logic swip;
  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  //
  // binary and gray pointers
  //
  assign wrptr_bin_next   = wrptr_bin + wrreq;
  assign wrptr_gray_next  = bin2gray(wrptr_bin_next);

  always_ff @(posedge wrclk or posedge reset) begin
    if (reset) begin
      wrptr_bin   <= '0;
      wrptr_gray  <= '0;
    end else if (wrclr) begin
      wrptr_bin   <= '0;
      wrptr_gray  <= '0;
    end else if (wrcen) begin
      wrptr_bin   <= wrptr_bin_next;
      wrptr_gray  <= wrptr_gray_next;
    end
  end
  //
  // read pointer syncronizer
  //
  always_ff @(posedge wrclk) begin
    {rdptr_gray_2, rdptr_gray_1} <= {rdptr_gray_1, rdptr_gray};
  end

  assign rdptr_bin = gray2bin(rdptr_gray_2);

  //
  // convert read pointer to write pointer width for flag decode loigc
  //
  generate
    //
    // if pRADDR_W >= pWADDR_W then pRDATA_W =< pWDATA_W and we must to convert
    //    read pointer to write pointer with cut off bits at left
    if (pRADDR_W > pWADDR_W) begin
      assign rdptr_bin2wrptr = rdptr_bin[pRADDR_W : pRADDR_W-pWADDR_W];
    end
    //
    // if pRADDR_W < pWADDR_W then pRDATA_W > pWDATA_W and we must to convert
    //    write pointer to read pointer with add 1'b0 at rigth
    else begin
      assign rdptr_bin2wrptr = {rdptr_bin, {(pWADDR_W-pRADDR_W){1'b0}}};
    end
  endgenerate

  assign rdptr_bin2wrptr_inv = {~rdptr_bin2wrptr[pWADDR_W], rdptr_bin2wrptr[pWADDR_W-1 : 0]};

  //
  // flag decode logic :: convert read pointer to write pointer width
  //
  always_ff @(posedge wrclk or posedge reset) begin
    if (reset) begin
      wrempty <= 1'b1;
      wrfull  <= 1'b0;
      wrusedw <= '0;
    end else if (wrclr) begin
      wrempty <= 1'b1;
      wrfull  <= 1'b0;
      wrusedw <= '0;
    end else begin
      wrempty <= (wrptr_bin       == rdptr_bin2wrptr);
      wrfull  <= (wrptr_bin_next  == rdptr_bin2wrptr_inv);
      wrusedw <= (wrptr_bin_next - rdptr_bin2wrptr);
    end
  end

endmodule

// synthesis translate_off
module tb ;

  logic         reset   ;
  logic         wrclk   ;
  logic         wrcen   ;
  logic         wrreq   ;
  logic [7 : 0] wrdata  ;
  logic         wrfull  ;
  logic         wrempty ;
  logic [5 : 0] wrusedw ;
  logic         rdclk   ;
  logic         rdcen   ;
  logic         rdreq   ;
  logic [7 : 0] rddata  ;
  logic         rdempty ;
  logic         rdfull  ;
  logic [5 : 0] rdusedw ;

  sfifo
  #(
    .pWDATA_W ( 8 ) ,
    .pWADDR_W ( 6 ) ,
    .pRDATA_W ( 8 ) ,
    .pRADDR_W ( 6 )
  )
  uut
  (
    .reset   ( reset   ) ,
    .wrclk   ( wrclk   ) ,
    .wrreq   ( wrreq & ~wrfull  ) ,
    .wrcen   ( wrcen   ) ,
    .wrdata  ( wrdata  ) ,
    .wrfull  ( wrfull  ) ,
    .wrempty ( wrempty ) ,
    .wrusedw ( wrusedw ) ,
    .rdclk   ( rdclk   ) ,
    .rdreq   ( rdreq   ) ,
    .rdcen   ( rdcen   ) ,
    .rddata  ( rddata  ) ,
    .rdempty ( rdempty ) ,
    .rdfull  ( rdfull  ) ,
    .rdusedw ( rdusedw )
  );

  initial begin : rst_gen
    reset = 1'b1;
    #10ns;
    reset = 1'b0;
  end

  initial begin : clk_gen
    wrclk = 1'b0;
    rdclk = 1'b0;
    fork
      #3ns  forever #3ns  wrclk = ~wrclk;
      #10ns forever #10ns rdclk = ~rdclk;
    join_none
  end

  assign wrcen = 1'b1;
  assign rdcen = 1'b1;

  initial begin : write
    wrreq   = 1'b0;
    wrdata  = 1'b0;
    wait (!reset);
    repeat (2) @(posedge wrclk);
    for (int i = 0; i < 60; i++) begin
      wrreq  <= #1ns 1'b1;
      wrdata <= #1ns i + 10;
      @(posedge wrclk iff !wrfull);
      wrreq  <= #1ns 1'b0;
    end

  end

  logic qvalid = 1'b0;
  logic [7:0] data_last;
  logic       ff = 1'b0;

  always_ff @(posedge rdclk) begin
    qvalid <= #1ns rdreq & rdcen;
  end

  always_ff @(posedge rdclk) begin
    if (qvalid) begin
      $display ("read %0d", rddata);
      if (ff) begin
        assert ((data_last + 1'b1) == rddata)
        else $error("data incorrect %0d, %0d", data_last, rddata);
      end
      data_last = rddata ;
      ff        = 1'b1;
    end
  end

  initial begin : read
    rdreq = 1'b0;
    wait (!reset);
    #100ns;
    repeat (2) @(posedge rdclk);
    for (int i = 0; i < 20; i++) begin
      rdreq <= #1ns 1'b1;
      @(posedge rdclk iff !rdempty);
      rdreq <= #1ns 1'b0;
      if (&i[1:0])
        repeat (3) @(posedge rdclk);
    end
    repeat (10) @(posedge rdclk);

    for (int i = 0; i < 20; i++) begin
      rdreq <= #1ns 1'b1;
      @(posedge rdclk iff !rdempty);
      rdreq <= #1ns 1'b0;
    end

    for (int i = 0; i < 20; i++) begin
      rdreq <= #1ns 1'b1;
      @(posedge rdclk iff !rdempty);
      rdreq <= #1ns 1'b0;
      repeat (2) @(posedge rdclk);
    end

    repeat (10) @(posedge rdclk);
    $stop;
  end


endmodule
// synthesis translate_on

