`ifndef _DPRAM_PARAM_
`define _DPRAM_PARAM_

module dpram_param #(
    parameter                          RAM_LEN  = 0,
    parameter                          DATA_WDTH  = 0,
    parameter                          ADDR_WDTH  = 0
)
(
	input [DATA_WDTH-1:0] data_a, data_b,
	input [ADDR_WDTH-1:0] addr_a, addr_b,
	input we_a, we_b, clk_a, clk_b,
	output reg [DATA_WDTH-1:0] q_a, q_b
);
	// Declare the RAM variable
	reg [DATA_WDTH-1:0] ram[RAM_LEN-1:0];
	
	// Port A
	always @ (posedge clk_a)
	begin
		if (we_a) 
		begin
			ram[addr_a] <= data_a;
			q_a <= data_a;
		end
		else 
		begin
			q_a <= ram[addr_a];
		end
	end
	
	// Port B
	always @ (posedge clk_b)
	begin
		if (we_b)
		begin
			ram[addr_b] <= data_b;
			q_b <= data_b;
		end
		else
		begin
			q_b <= ram[addr_b];
		end
	end
	
endmodule
`endif
