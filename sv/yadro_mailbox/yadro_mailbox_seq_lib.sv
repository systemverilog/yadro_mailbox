`ifndef __YADRO_MAILBOX_SEQ_LIB_SV__
`define __YADRO_MAILBOX_SEQ_LIB_SV__

// SEQUENCE: yadro_mailbox_write_byte_seq
class yadro_mailbox_write_byte_seq extends apb_master_base_seq;
  rand bit [`APB_ADDR_WDTH-1:0] addr_tmp;
  bit [`APB_ADDR_WDTH-1:0] addr_tmp2;	
  rand bit [7:0] write_data;
  int					N;
  int					p_index;	
  rand int unsigned transmit_del;

  function new(string name="yadro_mailbox_write_byte_seq");
    super.new(name);
  endfunction
 
  `uvm_object_utils(yadro_mailbox_write_byte_seq)

  constraint transmit_del_ct { (transmit_del <= 10); }
    
  virtual task body();
    `uvm_info(get_type_name(), "Starting...", UVM_HIGH)
    
//    	    `uvm_info("yadro_mailbox_write_byte_seq", $sformatf("CFG :\n%s",
//              p_sequencer.cfg.sprint()), UVM_MEDIUM)
//	    $stop;
    
    N 			= p_sequencer.cfg.N;
    p_index 	= p_sequencer.cfg.p_index;

    
	 std::randomize(addr_tmp) with {
		 addr_tmp[$clog2(`N)-1:0] 							== p_index;	 
		 addr_tmp[$clog2(`N**2)-1:$clog2(`N)] inside {[0:`N-1]};
		 addr_tmp[`APB_ADDR_WDTH-1:$clog2(`N**2)] 	== 0;
	 };
	 
    addr_tmp2 = addr_tmp;
    
    
    
    `uvm_info("yadro_mailbox_write_byte_seq", $sformatf("addr_tmp :\n%x",
              addr_tmp), UVM_MEDIUM)
    
    `uvm_do_with(req, 
      { 
	     req.addr 			== addr_tmp2;
        req.direction 	== APB_WRITE;
        req.data 			== write_data;
	     req.N 				== N; 
	     req.p_index 		== p_index; 
        req.transmit_delay == transmit_del; } )
//	 $stop;   
    `uvm_info(get_type_name(), $sformatf("addr = 'h%0h, data = 'h%0h", 
        req.addr, req.data), UVM_HIGH)
    endtask
endclass : yadro_mailbox_write_byte_seq


// SEQUENCE: yadro_mailbox_write_byte_seq_mass
class yadro_mailbox_write_byte_seq_mass extends apb_master_base_seq;
  rand bit [`APB_ADDR_WDTH-1:0] addr_tmp;
  bit [`APB_ADDR_WDTH-1:0] addr_tmp2;	
  rand bit [7:0] write_data;
  int					N;
  int					p_index;	
  rand int unsigned transmit_del;

  yadro_mailbox_write_byte_seq	yadro_mailbox_write_byte_seq_inst;

  function new(string name="yadro_mailbox_write_byte_seq_mass");
    super.new(name);
  endfunction
 
  `uvm_object_utils(yadro_mailbox_write_byte_seq_mass)

  constraint transmit_del_ct { (transmit_del <= 10); }
    
  virtual task body();
    `uvm_info(get_type_name(), "Starting...", UVM_HIGH)
    
    repeat (1000) begin
	 	`uvm_do(yadro_mailbox_write_byte_seq_inst);
	 end   
	    
    
    endtask
endclass : yadro_mailbox_write_byte_seq_mass


class yadro_mailbox_rd_ctrl_reg_seq extends uvm_sequence #(apb_transfer);
  bit [`APB_ADDR_WDTH-1:0] addr_tmp;
  bit [`APB_ADDR_WDTH-1:0] 		fifo_addr_tmp;
  int								N;
  int								p_index;
  int 							fifo_index;
  
  function new(string name="yadro_mailbox_rd_ctrl_reg_seq");
    super.new(name);
	 set_automatic_phase_objection(1); 
  endfunction

  `uvm_object_utils(yadro_mailbox_rd_ctrl_reg_seq)
  `uvm_declare_p_sequencer(apb_slave_sequencer)

  apb_transfer req;
  apb_transfer util_transfer;
  
  
  
  virtual task body();
    `uvm_info(get_type_name(), "Starting...", UVM_MEDIUM)
    
    forever begin
	    N 			= p_sequencer.cfg.N;
	    p_index 	= p_sequencer.cfg.p_index;
	    
	    addr_tmp = N**2+p_index;
	    
	    `uvm_do_with(req, { 
		 	 req.addr 		== addr_tmp;
		    req.direction == APB_READ;
		 } )
	    
	    get_response(rsp);
	    `uvm_info(get_type_name(),
	      $sformatf("%s read ctrl_reg : addr = `x%0h, data = `x%0h",
	      get_sequence_path(), rsp.addr, rsp.data), 
	      UVM_MEDIUM);
	    
	    for (int i = 0; i < $bits(rsp.data); i++) begin
		    if (rsp.data[i] == 1'b1) begin
			    fifo_index = i;
//			    `uvm_info(get_type_name(),
//				      $sformatf("%s TEST : i = %0d",
//				      get_sequence_path(), i), 
//				      UVM_MEDIUM);
			    break;
		    end
	    end
	    
//	    `uvm_info(get_type_name(),
//				      $sformatf("%s TEST2 : p_index = %0d",
//				      get_sequence_path(), p_index), 
//				      UVM_MEDIUM);
	    
	    fifo_addr_tmp[$clog2(`N)-1:0] 							= fifo_index;	 
		 fifo_addr_tmp[$clog2(`N**2)-1:$clog2(`N)] 			= p_index;
		 fifo_addr_tmp[`APB_ADDR_WDTH-1:$clog2(`N**2)] 		= 0;
	    
	    `uvm_do_with(req, { 
		 	 req.addr 		== fifo_addr_tmp;
		    req.direction == APB_READ;
	    } )
		 
		 get_response(rsp);
//	    $stop;
    end
    
  endtask : body

endclass : yadro_mailbox_rd_ctrl_reg_seq


`endif // APB_MASTER_SEQ_LIB_SV
