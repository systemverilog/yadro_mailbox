//------------------------------------------------------------------------------
//
// CLASS: yadro_mailbox_env
//
//------------------------------------------------------------------------------

class yadro_mailbox_env #(
	int W 				= 32,
	int N 				= 32,
	int K 				= 64,
	int APB_ADDR_WDTH = 32
) extends uvm_env;

  // Virtual Interface variable
  protected virtual interface yadro_mailbox_if#(.W(W), .N(N), .K(K), .APB_ADDR_WDTH(APB_ADDR_WDTH)) 	yadro_mailbox_vif;
  
  apb_pkg::apb_env						apb_env_inst[];
  apb_pkg::apb_scoreboard				apb_scoreboard_inst[][];	  
  apb_pkg::apb_config					cfg;
	  
	  
	  
//  ic_pkg::ic_mon							ic_mon_inst[];

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils_begin(yadro_mailbox_env#(.W(W), .N(N), .K(K), .APB_ADDR_WDTH(APB_ADDR_WDTH)))
//    `uvm_field_int(has_bus_monitor, UVM_DEFAULT)
//    `uvm_field_int(num_masters, UVM_DEFAULT)
//    `uvm_field_int(num_slaves, UVM_DEFAULT)
//    `uvm_field_int(intf_checks_enable, UVM_DEFAULT)
//    `uvm_field_int(intf_coverage_enable, UVM_DEFAULT)
  `uvm_component_utils_end

  // new - constructor
  function new(string name, uvm_component parent);
    super.new(name, parent);
	  
	 // Create the APB UVC configuration class if it has not been set
	  if(cfg == null) //begin
	    if (!uvm_config_db#(apb_pkg::apb_config)::get(this, "", "cfg", cfg)) begin
	    `uvm_info("NOCONFIG", "Using default_apb_config", UVM_MEDIUM)
	    $cast(cfg, create_object("default_apb_config","cfg"));
	  end 
  endfunction : new

  // build_phase
  function void build_phase(uvm_phase phase);
    string inst_name;
	 string cfg_inst_name; 
//    set_phase_domain("uvm");
    super.build_phase(phase);
     if(!uvm_config_db#(virtual yadro_mailbox_if#(.W(W), .N(N), .K(K), .APB_ADDR_WDTH(APB_ADDR_WDTH)))::get(this, "", "yadro_mailbox_vif", yadro_mailbox_vif))
       `uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".yadro_mailbox_vif"});
    
    
    apb_env_inst 	= new[N];
    apb_scoreboard_inst = new[N];
    foreach(apb_scoreboard_inst[i])
      apb_scoreboard_inst[i] = new[N];
    
//    ic_mon_inst	= new[N]; 
    for(int i = 0; i < N; i++) begin
      $sformat(inst_name, "apb_env_inst[%0d]", i);
	   uvm_config_object::set(this, inst_name, "cfg", cfg); 
	   uvm_config_db#(virtual apb_if#(.DATA_WIDTH(W),.ADDR_WIDTH(APB_ADDR_WDTH)))::set(this, inst_name, "vif", yadro_mailbox_vif.apbN_if[i]);
	   uvm_config_db#(virtual ic_if)::set(this, inst_name, "ic_vif", yadro_mailbox_vif.icN_if[i]);
	   
//	   $sformat(cfg_inst_name, "apb_env_inst_cfg[%0d]", i);
//	   apb_env_inst_cfg[i] = axi_st_pkg::cfg_c::type_id::create(cfg_inst_name);
//	   uvm_config_db#(axi_st_pkg::cfg_c)::set(this, inst_name, "cfg", apb_env_inst_cfg[i]);
	   
      apb_env_inst[i] = apb_pkg::apb_env#(.DATA_WIDTH(W),.ADDR_WIDTH(APB_ADDR_WDTH))::type_id::create(inst_name, this);
	   for(int j = 0; j < N; j++) begin
		   $sformat(inst_name, "apb_scoreboard_inst[%0d][%0d]", i, j);
	   	apb_scoreboard_inst[i][j] = apb_pkg::apb_scoreboard#(.DATA_WIDTH(W),.ADDR_WIDTH(APB_ADDR_WDTH))::type_id::create(inst_name, this);
		   void'(uvm_config_db#(int)::set(this,inst_name, "p_i", i));
		   void'(uvm_config_db#(int)::set(this,inst_name, "p_j", j));
	   end
    end    
  endfunction : build_phase

//	// UVM connect_phase
	function void connect_phase(uvm_phase phase);
	  for(int i = 0; i < N; i++) begin
		  	for(int j = 0; j < N; j++) begin  
	  			apb_env_inst[i].masters[0].monitor.item_collected_port.connect(apb_scoreboard_inst[j][i].sent_imp);
		  		apb_env_inst[i].slaves[0].monitor.item_collected_port.connect(apb_scoreboard_inst[i][j].rcvd_imp);		  
		   end
	  end	  
	endfunction : connect_phase

endclass : yadro_mailbox_env


