`ifndef __YADRO_MAILBOX_MACROS__
`define __YADRO_MAILBOX_MACROS__

	`define CLOG2(x) \
	   (x <= 2) ? 1 : \
	   (x <= 4) ? 2 : \
	   (x <= 8) ? 3 : \
	   (x <= 16) ? 4 : \
	   (x <= 32) ? 5 : \
	   (x <= 64) ? 6 : \
	   (x <= 128) ? 7 : \
	   (x <= 256) ? 8 : \
	   (x <= 512) ? 9 : \
	   (x <=	1024) ? 10 : \
	   -1
	
	`define W 			32
	`define N			8
	`define K			64
	
	`define APB_ADDR_WDTH	32
	`define N_ADDR_WDTH `CLOG2(`N**2)
	`define K_ADDR_WDTH `CLOG2(`K)
	
`endif
