package yadro_mailbox_pkg;

	import uvm_pkg::*;

	`include "uvm_macros.svh"

	typedef uvm_config_db#(virtual yadro_mailbox_if) yadro_mailbox_vif_config;
	typedef virtual yadro_mailbox_if yadro_mailbox_vif;

	`include "yadro_mailbox_macros.sv"
	`include "yadro_mailbox_env.sv"

endpackage: yadro_mailbox_pkg

