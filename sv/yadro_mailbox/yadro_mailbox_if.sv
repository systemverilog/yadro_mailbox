`ifndef __YADRO_MAILBOX_IF__
`define __YADRO_MAILBOX_IF__

`include "apb_if.sv"
`include "ic_if.sv"

interface yadro_mailbox_if #(
	parameter W 				= 32,
	parameter N 				= 32,
	parameter K 				= 64,
	parameter APB_ADDR_WDTH = 32
)();
	
	logic              		clk;
   logic              		reset;
	
	apb_if #(.DATA_WIDTH(W), .ADDR_WIDTH(APB_ADDR_WDTH))	apbN_if[N-1:0](.PCLK(clk), .PRESETn(~reset));
	
	ic_if 	icN_if[N-1:0](.clk(clk), .reset(reset));
	
endinterface : yadro_mailbox_if
`endif
