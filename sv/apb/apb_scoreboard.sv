//------------------------------------------------------------------------------
//
// CLASS: apb_scoreboard
//
//------------------------------------------------------------------------------
class apb_scoreboard #(
		int DATA_WIDTH = 32,
		int ADDR_WIDTH = 32
	) extends uvm_scoreboard;
  `uvm_analysis_imp_decl(_sent)
  `uvm_analysis_imp_decl(_rcvd)
  
//  cfg_c				cfg;

  uvm_analysis_imp_sent #(apb_transfer#(DATA_WIDTH, ADDR_WIDTH), apb_scoreboard) sent_imp;
  uvm_analysis_imp_rcvd #(apb_transfer#(DATA_WIDTH, ADDR_WIDTH), apb_scoreboard) rcvd_imp;

  apb_transfer#(DATA_WIDTH, ADDR_WIDTH) 	exp_que[$];
  
  protected int p_i;
  protected int p_j;	
  
  protected int	push_cond;
  protected int	pop_cond;	
  
  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils_begin(apb_scoreboard#(DATA_WIDTH, ADDR_WIDTH))
//    `uvm_field_object		(cfg, 				UVM_DEFAULT | UVM_REFERENCE)
	  `uvm_field_int(p_i, UVM_DEFAULT)
	  `uvm_field_int(p_j, UVM_DEFAULT)
  `uvm_component_utils_end

  // new - constructor
  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  //build_phase
  function void build_phase(uvm_phase phase);
    sent_imp = new("sent_imp", this);
    rcvd_imp = new("rcvd_imp", this);
	 
	 void'(uvm_config_db#(int)::get(this, "", "p_i", p_i));
	 void'(uvm_config_db#(int)::get(this, "", "p_j", p_j)); 
  endfunction

  // write_bu_afar
  virtual function void write_sent(apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
	  apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans_tmp;
////	  trans.print();
//	  `uvm_info("write_sent", $sformatf("%s",
//            trans.sprint()), UVM_LOW)
//	  
//	  $stop;
//	  trans_tmp = trans;
//	  `uvm_info("write_sent", $sformatf("%s",
//            trans_tmp.sprint()), UVM_LOW)
//	  $stop;

	 
	 trans_tmp = apb_transfer#(DATA_WIDTH, ADDR_WIDTH)::type_id::create("trans_tmp",this);
    trans_tmp.copy(trans);
	 
	 push_cond = (trans_tmp.direction == APB_WRITE) & (trans_tmp.addr[$clog2(`N)-1:0] == p_j) & (trans_tmp.addr[$clog2(`N**2)-1:$clog2(`N)] == p_i);
//		  `uvm_info("write_sent", $sformatf("%s",
//	            trans_tmp.sprint()), UVM_LOW)
//
//		  `uvm_info("write_sent", $sformatf("addr[clog2(N)-1:0] = %0d, addr[clog2(N**2)-1:clog2(N)] = %0d",
//	            trans_tmp.addr[$clog2(`N)-1:0], trans_tmp.addr[$clog2(`N**2)-1:$clog2(`N)]), UVM_LOW)
//		  
//		  `uvm_info("write_sent", $sformatf("p_i = %0d, p_j = %0d",
//	            p_i, p_j), UVM_LOW)
//		
//		  `uvm_info("write_sent", $sformatf("push_cond = %0d",
//	            push_cond), UVM_LOW)

	 
	 if (push_cond) begin
	    exp_que.push_back(trans_tmp);
//		 if (trans_tmp.p_index == 0) begin
//		  `uvm_info("write_sent", $sformatf("%s",
//	            trans.sprint()), UVM_LOW)
//			 $stop; 
//		 end	  
	 end 

  endfunction : write_sent

  // write_bu_afar
  virtual function void write_rcvd(apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
	apb_transfer#(DATA_WIDTH, ADDR_WIDTH)	exp_trans;
	//trans.print();
	//`uvm_info(get_type_name(),$sformatf("111 EXP_QUE SIZE = %d", exp_que.size()), UVM_LOW)
	
	pop_cond = (trans.direction == APB_READ) & 
						(trans.addr[$clog2(`N)-1:0] == p_j) & (trans.addr[$clog2(`N**2)-1:$clog2(`N)] == p_i) &
						(trans.addr[`APB_ADDR_WDTH-1:$clog2(`N**2)] 		== 0); 
	
//	`uvm_info("write_rcvd", $sformatf("%s",
//	            trans.sprint()), UVM_LOW)
		
	
//	if ((trans.direction == APB_READ) && (trans.addr[`APB_ADDR_WDTH-1:$clog2(`N**2)] == 0)) begin
		   
//			`uvm_info("write_rcvd", $sformatf("%s",
//	            trans.sprint()), UVM_LOW)
//		  
//		  	`uvm_info("write_rcvd", $sformatf("addr[clog2(N)-1:0] = %0d, addr[clog2(N**2)-1:clog2(N)] = %0d, , addr[APB_ADDR_WDTH-1:clog2(N**2)] = %0d",
//	            trans.addr[$clog2(`N)-1:0], trans.addr[$clog2(`N**2)-1:$clog2(`N)], trans.addr[`APB_ADDR_WDTH-1:$clog2(`N**2)] ), UVM_LOW)
//		  
//		  `uvm_info("write_rcvd", $sformatf("p_i = %0d, p_j = %0d",
//	            p_i, p_j), UVM_LOW)
//		
//		  `uvm_info("write_rcvd", $sformatf("pop_cond = %0d",
//	            pop_cond), UVM_LOW)	
//			 
//			  $stop;
//	end
	
	if (exp_que.size() && pop_cond) begin
		exp_trans = exp_que.pop_front();
		
//		if (trans.p_index == 4) begin
//			`uvm_info("write_rcvd trans", $sformatf("%s",
//	            trans.sprint()), UVM_LOW)
//			$stop;	
//		end
		
		`uvm_info("write_rcvd trans", $sformatf("%s",
	            trans.sprint()), UVM_LOW)
		
		`uvm_info("write_rcvd exp_trans", $sformatf("%s",
	            exp_trans.sprint()), UVM_LOW)
		
		

		if (trans.data !== exp_trans.data) begin
			`uvm_info(get_type_name(),$sformatf("MISMATCH: expd: %0h",  exp_trans.data), UVM_LOW)
			`uvm_info(get_type_name(),$sformatf("MISMATCH: rcvd: %0h",  trans.data), UVM_LOW)
			$stop;
		end else begin
			`uvm_info(get_type_name(),$sformatf("Processor[%0d] to Processor[%0d] transfer - OK! Msg: %0h", exp_trans.p_index, trans.p_index, trans.data), UVM_LOW)
//			$stop;
		end
//	end else begin
//		`uvm_info(get_type_name(),$sformatf("No more data"), UVM_LOW)
	end

  endfunction : write_rcvd

  // report_phase
//  virtual function void report_phase(uvm_phase phase);
//    if(!disable_scoreboard) begin
//      `uvm_info(get_type_name(),
//        $sformatf("Reporting scoreboard information...\n%s", this.sprint()), UVM_LOW)
//    end
//  endfunction : report_phase

endclass : apb_scoreboard


