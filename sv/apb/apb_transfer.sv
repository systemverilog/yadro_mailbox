//------------------------------------------------------------------------------
// CLASS: apb_transfer declaration
//------------------------------------------------------------------------------

class apb_transfer #(
		int DATA_WIDTH = 32,
		int ADDR_WIDTH = 32
	) extends uvm_sequence_item;                                  

  rand bit [ADDR_WIDTH-1:0]   addr;
  rand apb_direction_enum   	direction;
  rand bit [DATA_WIDTH-1:0]   data;
  rand int unsigned         	transmit_delay;
  string                    	master;
  string                    	slave;
  int									N;
  int									p_index;
  
  constraint c_direction { direction inside { APB_READ, APB_WRITE }; }
 
  constraint c_transmit_delay { transmit_delay <= 10 ; }
  
//  constraint order { solve p_index before addr ;}
//  
//  constraint c_addr { 
//	  addr[$clog2(`N)-1:0] == p_index;
//	  addr[$clog2(`N*2)-1:$clog2(`N)] inside {[0:`N-1]};
//	  addr[ADDR_WIDTH-1:$clog2(`N*2)] == 0;
//  }
  
  `uvm_object_utils_begin(apb_transfer#(DATA_WIDTH, ADDR_WIDTH))
    `uvm_field_int(addr, UVM_DEFAULT)
    `uvm_field_enum(apb_direction_enum, direction, UVM_DEFAULT)
    `uvm_field_int(data, UVM_DEFAULT)
    `uvm_field_string(master, UVM_DEFAULT|UVM_NOCOMPARE)
    `uvm_field_string(slave, UVM_DEFAULT|UVM_NOCOMPARE)
    `uvm_field_int(N, UVM_DEFAULT)
    `uvm_field_int(p_index, UVM_DEFAULT)
  `uvm_object_utils_end

  function new (string name = "apb_transfer");
    super.new(name);
  endfunction
  
  virtual function void do_copy(uvm_object rhs);
	   apb_transfer#(DATA_WIDTH, ADDR_WIDTH) 	rhs_;
	   if(!$cast(rhs_, rhs))
					`uvm_error("do_copy", "cast failed, check type compatability")
	   super.do_copy(rhs);
	   addr      		= rhs_.addr;
	   direction     	= rhs_.direction;
	   data    			= rhs_.data;
	   transmit_delay = rhs_.transmit_delay; 
	   master 			= rhs_.master;
		slave 			= rhs_.slave;
		N 					= rhs_.N;
		p_index 			= rhs_.p_index;  
  endfunction
  
endclass : apb_transfer
