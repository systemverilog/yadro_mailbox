/*******************************************************************************
  FILE : apb_slave_driver.sv
*******************************************************************************/
//   Copyright 1999-2010 Cadence Design Systems, Inc.
//   All Rights Reserved Worldwide
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//----------------------------------------------------------------------


`ifndef APB_SLAVE_DRIVER_SV
`define APB_SLAVE_DRIVER_SV

//------------------------------------------------------------------------------
// CLASS: apb_slave_driver declaration
//------------------------------------------------------------------------------

class apb_slave_driver #(
		int DATA_WIDTH = 32,
		int ADDR_WIDTH = 32
	) extends uvm_driver #(apb_transfer);

  // The virtual interface used to drive and view HDL signals.
  virtual apb_if#(.DATA_WIDTH(DATA_WIDTH),.ADDR_WIDTH(ADDR_WIDTH)) vif;
  virtual ic_if 		ic_vif;	
    
  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils(apb_slave_driver)
  
  // Constructor
  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  // Additional class methods
  extern virtual function void connect_phase(uvm_phase phase);
  extern virtual task run_phase(uvm_phase phase);
  extern virtual task get_and_drive();
  extern virtual task reset_signals();
  extern virtual protected task drive_transfer (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
  extern virtual protected task drive_address_phase (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
  extern virtual protected task drive_data_phase (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);

endclass : apb_slave_driver

// UVM connect_phase - gets the vif as a config property
function void apb_slave_driver::connect_phase(uvm_phase phase);
  super.connect_phase(phase);
    if (!uvm_config_db#(virtual apb_if#(.DATA_WIDTH(DATA_WIDTH),.ADDR_WIDTH(ADDR_WIDTH)))::get(this, "", "vif", vif))
      `uvm_error("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"})
    
    if (!uvm_config_db#(virtual ic_if)::get(this, "", "ic_vif", ic_vif))
      `uvm_error("NOVIF",{"virtual interface must be set for: ",get_full_name(),".ic_vif"})
endfunction : connect_phase

// Externed virtual declaration of the run_phase method.  This method
// fork/join_none's the get_and_drive() and reset_signals() methods.
task apb_slave_driver::run_phase(uvm_phase phase);
  fork
    get_and_drive();
    reset_signals();
  join
endtask : run_phase

// Function that manages the interaction between the slave
// sequence sequencer and this slave driver.
task apb_slave_driver::get_and_drive();
  @(posedge vif.PRESETn);
  `uvm_info(get_type_name(), "Reset dropped", UVM_MEDIUM)
  forever begin
	 wait(ic_vif.interrupt);
	 `uvm_info(get_type_name(), "INTERRUPT!!!", UVM_MEDIUM) 
    seq_item_port.get_next_item(req);
    drive_transfer(req);
    seq_item_port.item_done();
	 seq_item_port.put_response(req); 
  end
endtask : get_and_drive

// Process task that assigns signals to reset state when reset signal set
task apb_slave_driver::reset_signals();
  forever begin
    @(negedge vif.PRESETn);
    `uvm_info(get_type_name(), "Reset observed",  UVM_MEDIUM)
    vif.PRDATA <= 32'hzzzz_zzzz;
    vif.PREADY <= 0;
    vif.PSLVERR <= 0;
  end
endtask : reset_signals

// Drives a transfer when an item is ready to be sent.
task apb_slave_driver::drive_transfer (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
  void'(begin_tr(trans, "apb slave driver", "UVM Debug",
       "APB master driver transaction from get_and_drive"));
  if (trans.transmit_delay > 0) begin
    repeat(trans.transmit_delay) @(posedge vif.PCLK);
  end
  while (vif.PSEL) begin
    	@(posedge vif.PCLK);
  end
  drive_address_phase(trans);
  drive_data_phase(trans);
  // apb_slave_driver_TR tag required for Debug Labs
  `uvm_info("apb_slave_driver_TR", $sformatf("APB Slave Finished Driving Transfer \n%s",
            trans.sprint()), UVM_MEDIUM)
  end_tr(trans);
  
endtask : drive_transfer

// Drive the address phase of the transfer
task apb_slave_driver::drive_address_phase (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
//  int slave_indx;
//  slave_indx = cfg.get_slave_psel_by_addr(trans.addr);
  vif.PADDR <= trans.addr;
//  vif.PSEL <= (1<<slave_indx);
  vif.PSEL <= 1'b1;	
  vif.PENABLE <= 0;
  if (trans.direction == APB_READ) begin
    vif.PWRITE <= 1'b0;
  end    
  else begin
    vif.PWRITE <= 1'b1;
    vif.PWDATA <= trans.data;
  end
  @(posedge vif.PCLK);
endtask : drive_address_phase

// Drive the data phase of the transfer
task apb_slave_driver::drive_data_phase (apb_transfer#(DATA_WIDTH, ADDR_WIDTH) trans);
  vif.PENABLE <= 1;
  @(posedge vif.PCLK iff vif.PREADY); 
  if (trans.direction == APB_READ) begin
    trans.data = vif.PRDATA;
  end
  vif.PENABLE <= 0;
  vif.PSEL    <= 0;
  @(posedge vif.PCLK);
endtask : drive_data_phase

`endif // APB_SLAVE_DRIVER_SV
