interface apb_if #(
	parameter DATA_WIDTH = 8,
	parameter ADDR_WIDTH = 8
) (
	input PCLK,
   input PRESETn
);

  logic [ADDR_WIDTH-1:0] 	PADDR;
  logic [DATA_WIDTH-1:0] 	PRDATA;
  logic [DATA_WIDTH-1:0] 	PWDATA;
  logic 							PSEL; // Only connect the ones that are needed
  logic 							PENABLE;
  logic 							PWRITE;
  logic 							PREADY;
  logic 							PSLVERR;

  property psel_valid;
    @(posedge PCLK)
    !$isunknown(PSEL);
  endproperty: psel_valid

  CHK_PSEL: assert property(psel_valid);

  COVER_PSEL: cover property(psel_valid);

endinterface: apb_if