//------------------------------------------------------------------------------
// CLASS: ic_transfer declaration
//------------------------------------------------------------------------------

class ic_transfer extends uvm_sequence_item;                                  

  rand bit    						interrupt;
  
  `uvm_object_utils_begin(ic_transfer)
    `uvm_field_int(interrupt, UVM_DEFAULT)
  `uvm_object_utils_end

  function new (string name = "ic_transfer");
    super.new(name);
  endfunction

endclass : ic_transfer
