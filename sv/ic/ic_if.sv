`ifndef __IC_IF__
`define __IC_IF__

interface ic_if (
	input clk,
   input reset
);
	
	logic				interrupt;
	
endinterface : ic_if
`endif
