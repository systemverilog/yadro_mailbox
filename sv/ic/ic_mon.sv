`ifndef __IC_MON__
`define __IC_MON__

//------------------------------------------------------------------------------
// CLASS: ic_mon
//------------------------------------------------------------------------------

class ic_mon extends uvm_component;

  // The virtual interface used to view HDL signals.
  virtual ic_if 				vif;
  
  // IC configuration information
//  ic_config cfg;

  // Property indicating the number of transactions occuring on the IC.
  protected int unsigned num_transactions = 0;

  // The following two bits are used to control whether checks and coverage are
  // done both in the collector class and the interface.
  bit checks_enable = 1; 
  bit coverage_enable = 1;

  // TLM Ports - transfer collected for monitor other components
  uvm_analysis_port #(ic_transfer) item_collected_port;

  // The following property holds the transaction information currently
  // being captured (by the collect_address_phase and data_phase methods). 
  ic_transfer trans_collected;

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils_begin(ic_mon)
//    `uvm_field_object(cfg, UVM_DEFAULT|UVM_REFERENCE)
    `uvm_field_int(checks_enable, UVM_DEFAULT)
    `uvm_field_int(coverage_enable, UVM_DEFAULT)
    `uvm_field_int(num_transactions, UVM_DEFAULT)
  `uvm_component_utils_end

  // new - constructor
  function new (string name, uvm_component parent);
    super.new(name, parent);
    trans_collected = ic_transfer::type_id::create("trans_collected");
    // TLM ports are created here
    item_collected_port = new("item_collected_port", this);
  endfunction : new

  // Additional class methods
  extern virtual function void build_phase(uvm_phase phase);
  extern virtual function void connect_phase(uvm_phase phase);
  extern task run_phase(uvm_phase phase);
  extern virtual protected task collect_transactions();
  extern virtual function void report_phase(uvm_phase phase);

endclass : ic_mon

// UVM build_phase
function void ic_mon::build_phase(uvm_phase phase);
    super.build_phase(phase);
endfunction : build_phase

// UVM connect_phase
function void ic_mon::connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    if (!uvm_config_db#(virtual ic_if)::get(this, "", "vif", vif))
      `uvm_error("NOVIF", {"virtual interface must be set for: ", get_full_name(), ".vif"})
endfunction : connect_phase


// UVM run_phase()
task ic_mon::run_phase(uvm_phase phase);
    @(negedge vif.reset);
    `uvm_info("IC_MON", "Detected Reset Done", UVM_LOW)
    collect_transactions();
endtask : run_phase

// collect_transactions
task ic_mon::collect_transactions();
  forever begin
    @(posedge vif.clk iff (vif.interrupt != 0));
    void'(this.begin_tr(trans_collected, "ic_mon", "UVM Debug", 
           "IC collector transaction inside collect_transactions()"));
    trans_collected.interrupt = vif.interrupt;
    this.end_tr(trans_collected);
    item_collected_port.write(trans_collected);
    `uvm_info("IC_MON", $sformatf("Transfer collected :\n%s",
              trans_collected.sprint()), UVM_MEDIUM)
     num_transactions++;
    end
endtask : collect_transactions

function void ic_mon::report_phase(uvm_phase phase);
  super.report_phase(phase);
  `uvm_info("REPORT", $sformatf("IC collector collected %0d transfers", num_transactions), UVM_LOW);
endfunction : report_phase

`endif // APB_COLLECTOR_SV
