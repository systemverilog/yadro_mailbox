package ic_pkg;

	import uvm_pkg::*;

	`include "uvm_macros.svh"

	typedef uvm_config_db#(virtual ic_if) ic_vif_config;
	typedef virtual ic_if ic_vif;

	`include "ic_transfer.sv"
	`include "ic_mon.sv"

endpackage: ic_pkg

